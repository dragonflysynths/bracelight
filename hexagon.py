from PIL import Image
from math import *

three69 = sqrt(3);
thickness = 28;
unit_size = 25;
proximity = 13;

box_length = unit_size*3;
box_height = unit_size*2*sin(radians(60));

def processImage(filename):
    gerb = Image.open(filename);
    gerbpix = gerb.load();
    gerb_width, gerb_height = gerb.size;

    # PIL accesses images in Cartesian co-ordinates, so it is Image[columns, rows]
    img = Image.new( 'RGB', (gerb_width,gerb_height), "white") # create a new white image
    #img = Image.new( 'RGB', (1000,1000), "white") # create a new white image
    pixels = img.load() # create the pixel map

    # one unit box looks like
    # _    _
    #  \__/
    #  /  \

    # if the quadrant is normalized (flip symmetry), each section looks like
    # _
    #  \_
    #
    # where both horizontal lines are unitsize/2 long

    for y in range(gerb_height):    # For every row
        print float(y)/gerb_height;
        for x in range(gerb_width):    # for every col:
            # find relative location within box
            ypos = y % box_height;
            xpos = x % box_length;
            # normalize quadrant
            if (ypos > box_height/2):
                ypos = box_height - ypos;
            if (xpos > box_length/2):
                xpos = box_length - xpos;


            # warning track, or standard hexagon
            if (
                (not square_detect(x,y, proximity, gerb_width, gerb_height, gerbpix))
                and
                (square_detect(x,y, proximity + thickness, gerb_width, gerb_height, gerbpix)
                or
                (xpos < unit_size/2 and ypos < thickness/2) # top line
                or
                (xpos > (box_length - unit_size)/2 and ypos > (box_height - thickness)/2) # bottom line
                or
                (abs(ypos - three69*(xpos - unit_size/2)) < thickness) # diagonal defined by y = .5sqrt(3)(x - unit)
                )):
                pixels[x,y] = (0, 0, 0) # set the colour accordingly

    #img.show()
    img.save(filename + '.bmp')



# square detection for simplicity
def square_detect(x, y, width, gerb_width, gerb_height, gerbpix):
    xmin = x - width;
    if xmin < 0:
        xmin = 0;
    ymin = y - width;
    if ymin < 0:
        ymin = 0;
    xmax = x + width;
    if xmax >= gerb_width:
        xmax = gerb_width-1;
    ymax = y + width;
    if ymax >= gerb_height:
        ymax = gerb_height-1

    # allow to skip 5 pixels. assumes 8 pix width
        
    #print xmax
    # horizontals
    for thisx in range(xmin, xmax, 5):
        if (gerbpix[thisx, ymin] != (0,0,0) or
            gerbpix[thisx, ymax] != (0,0,0)):
            return True;
        
    # verticals
    for thisy in range(ymin, ymax, 5):
        if (gerbpix[xmin, thisy] != (0,0,0) or
            gerbpix[xmax, thisy] != (0,0,0)):
            return True;
        
    return False;
        

processImage("cu_top.png")
processImage("cu_bot.png")
